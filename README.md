# CMake Pardiso

The project demonstrates how to integrate pardiso into your CMake project.

Request a license key [here](https://www.pardiso-project.org/#download).

Option 1: Install the pardiso library with cmake. Provide your license 
key to cmake and specify the location where the pardiso 
library will be installed via the cmake variable `PARDISO_LIBRARY_DIR`. 
The directory PARDISO_LIBRARY_DIR will be created if it does not 
exist yet:

`cmake .. -DPARDISO_LIC_KEY=3499655142F33F994CF1792699F41534A832A6931DE83F438B0DUMMY -DPARDISO_LIBRARY_DIR=$HOME/code/libraries/pardiso`

On old linux systems (e.g., CentOS Linux release 7.7.1908) try adding the option
`-DPARDISO_GCC720=ON` to download a pardiso library that links against glibc, v2.27.

Option 2: The pardiso library was previously installed to the 
directory `PARDISO_LIBRARY_DIR` (either manually or with Option 1). 
A `pardiso.lic` file containing the license
will not be created, and you do not provide a key:

`cmake .. -DPARDISO_LIBRARY_DIR=$HOME/code/libraries/pardiso`

`PARDISO_LIBRARY_DIR` must be an absolute path.

Here is a full list of commands:

~~~~~~~~~~~~{sh}
module load gcc/7 intel cmake
mkdir -p buid && cd build
cmake .. -DPARDISO_LIC_KEY=3499655142F33F994CF1792699F41534A832A6931DE83F438B0DUMMY -DPARDISO_LIBRARY_DIR="$HOME/code/libraries/pardiso"
make
PARDISO_LIC_PATH="$HOME/code/libraries/pardiso" OMP_NUM_THREADS=8 ./linear_system_solve_cxx
### expected output is:
# [...]
#The solution of the system is: 
# x [0] = -0.211560
# x [1] = -0.285096
# x [2] = -0.200197
# x [3] =  0.362866
# x [4] =  0.730596
# x [5] =  0.661692
# x [6] =  0.051104
# x [7] =  0.938505 
PARDISO_LIC_PATH="$HOME/code/libraries/pardiso" OMP_NUM_THREADS=8 ./linear_system_solve_c
### expected output is:
# [...]
#Compute Diagonal Elements of the inverse of A ...
# [PARDISO]: Time selected inversion              1.4941692352294922E-003
#Diagonal element of A^{-1} =   1.428571428571428492126927e-01 =    1.428571428571428492126927e-01
#Diagonal element of A^{-1} =   1.250000000000678521454544e-13 =   -6.820806930902956474311970e-26
#Diagonal element of A^{-1} =   2.500000000000113797860024e-01 =    2.500000000000000000000000e-01
#Diagonal element of A^{-1} =   1.428571428571428492126927e-01 =    1.428571428571428492126927e-01
#Diagonal element of A^{-1} =   5.000000000002727817971504e-01 =   -5.454545454545456362183131e-01
#Diagonal element of A^{-1} =  -9.833333333333539316711835e-01 =    3.333333333333333148296163e-01
#Diagonal element of A^{-1} =  -1.136363636364253215665239e-14 =    9.090909090909091161414324e-02
#Diagonal element of A^{-1} =   1.499999999999977184916844e-01 =    4.999999999999999583666366e-02
~~~~~~~~~~~~
